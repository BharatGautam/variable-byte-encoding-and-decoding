/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package variable.pkgbyte.encoding1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Bharat Gautam
 */
public class VariableByteEncoding1 {

    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);
        while (true) {
        System.out.println("please enter 1 for encode or 2 for decode or 0 for exit");
         int n = scanner.nextInt();
        if(Objects.equals(n,1)){
             Encode();
        }
          else if (Objects.equals(n, 2)) {
            Decode();
        } else {
               break;           

        }
        }
        
        }
//        int n = sc.nextInt();
//        if (Objects.equals(n, 1)) {
//            Encode();
//        } else if (Objects.equals(n, 2)) {
//            Decode();
//        } else {
//            System.out.println("enter 0 to exit");
//
//        }

    

    public static void Encode() {

        System.out.println("please enter the number:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Integer> list = new ArrayList<>();
        int remainder2;

        int dividend = n, divisor = 128;
        int quotient = dividend / divisor;
        int remainder1 = dividend % divisor;
        int finalquotient = 0;
        int finalremainder1 = 0;
//        int  a=(int)(log(n)%log(128))+1;
//for calculating the remainder and the quotient and applying the condition
        while (quotient > 128) {
            remainder2 = quotient % divisor;
            quotient = quotient / divisor;
            list.add(remainder2);
            if (quotient < 128) {
                finalquotient = quotient;
                finalremainder1 = remainder1;

            }
        }

        System.out.println(finalquotient);
        int length = list.size();
        for (int i = length - 1; i >= 0; i--) {
            System.out.println(list.get(i));

        }
        System.out.println(finalremainder1);
        System.out.print("The Variable Byte Encoding is : ");
        Convert(finalquotient, false);
        //for getting the binary equivalent of the remainder2 in the list
        for (int i = length - 1; i >= 0; i--) {
            Convert(list.get(i), false);

        }

        Convert(finalremainder1, true);

    }

    public static void Convert(int num, boolean isremainder) {
        //for converting decimal value(ie remainder and quotient value) to binary 
        String str = "";
        while (num > 0) {
            int y = num % 2;
            str = y + str;
            num = num / 2;
        }

//for  adding  0 in the given bit 
        if (str.length() < 8) {
            int count = 8 - str.length();
            while (count > 0) {
                str = "0" + str;
                count--;
            }
            //for adding 1 to the last bit 
            if (isremainder) {
                str = str.substring(1);
                str = "1" + str;
            }
//              System.out.print(str+"");
         
        }
          System.out.print(str); 
    }

    public static void Decode() {

        System.out.println("please enter the given variable byte encoded number:");
        Scanner sc = new Scanner(System.in);
        String str = sc.next();

        List<Integer> list = new ArrayList<>();
        while (str.length() > 7) {
            String a = str.substring(0, 8);
            str = str.substring(8, str.length());

            if (str.length() == 0) {
                list.add(removeZero(a, true));
            } else {
                list.add(removeZero(a, false));
            }

//               int number=(list.get(0)*128+list.get(1)*128);
//            System.out.print(list);

        }
        int b = list.size();
        int res = 0;
        for (int i = 0; i < b; i++) {
            if (i == b - 1) {

                res += list.get(i);

            } else {
                if (i == 0) {
                    res += (list.get(i) * 128 + list.get(++i));
                } else {
                    res = ((res * 128) + list.get(i)) * 128;
                }
            }
        }
        System.out.print("the required Variable byte decoding  is :" + res);

    }

    public static int removeZero(String str, boolean isremainder) {
        //for the last bit we have 1 as MSB so to remove it we are using this condition 
        if (isremainder) {
            str = str.substring(1);
        }
        // Count leading zeros 
        int i = 0;
        while (str.charAt(i) == '0') {
            i++;
        }

        // Convert str into StringBuffer as Strings are immutable. 
        StringBuilder sb = new StringBuilder(str);

        // The  StringBuffer replace function removes i characters from given index (0 here) 
        sb.replace(0, i, "");

        //return the binary value into decimal equivalent
        return (Integer.parseInt(sb.toString(), 2));

    }

}

//00001110011000010011110111010100
//it split the given string into  string array of size 8
             //String a=  Arrays.toString(str.split("(?<=\\G.{8})"));
